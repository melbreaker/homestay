<?php
App::uses('FormsUser', 'Model');

/**
 * FormsUser Test Case
 *
 */
class FormsUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.forms_user',
		'app.user',
		'app.result',
		'app.form',
		'app.homestay',
		'app.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FormsUser = ClassRegistry::init('FormsUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FormsUser);

		parent::tearDown();
	}

}
