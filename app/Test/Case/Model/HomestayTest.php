<?php
App::uses('Homestay', 'Model');

/**
 * Homestay Test Case
 *
 */
class HomestayTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.homestay',
		'app.form',
		'app.category',
		'app.user',
		'app.result',
		'app.forms_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Homestay = ClassRegistry::init('Homestay');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Homestay);

		parent::tearDown();
	}

}
