$('#category-submit').click(function(){
	var category = $('#category-input').val();
	var formId = $('#form-id').val();
	$.ajax({
		type: "POST",
		url: "/homestay/forms/addCategory",
		data: {id: formId,category: category},
		dataType: 'json'
	}).done(function(data){
		if(data.success == false) {
			alert(data.cause);
		} else {
			var list = '';
			$('#category-modal').modal('hide');
			for (var i = 0; i < data.data.length; i++) {
				//console.log(data.data[i]['Category']['title']);
				list = list + "<li><a href='#'>"+data.data[i]['Category']['title']+"</a></li>"
			}
			$("#category-list").empty();
			$("#category-list").html(list);
		}
	});
});