<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property Result $Result
 * @property Form $Form
 */
class User extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'username' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณากรอกชื่อผู้ใช้'
			),
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Usernames must only contain letters and numbers.',
			),
			'usernameLength' => array(
                'rule'    => array('between', 8, 15),
                'message' => 'Usernames must be between 8 and 15 characters long.'
            ),
            'notDuplicate' => array(
                'rule' => array('isUnique'),
                'message' => 'This username has already been taken.'
            )
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณากรอกชื่อ'
			),
		),
		'surname' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณากรอกนามสกุล'	
			),
		),
		'position' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณาเลือกตำแหน่ง'
			)
		),
		'institution' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณากรอกหน่วยงาน'
			)
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'กรุณากรอกอีเมล'
			),
            'notDuplicate' => array(
                 'rule' => array('isUnique'),
                 'message' => 'This email already exists in the system',
            ),
		),
		'password' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณากรอกพาสเวิร์ด',
			),
			'usernameLength' => array(
		        'rule'    => array('between', 6, 15),
		        'message' => 'Password must be between 6 and 15 characters long.'
		    ),
            'passRule' => array(
                'rule' => array('custom', '`^(?=.*[^a-zA-Z0-9])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$`'),
                'message' => 'Password must contain at least 1 upper case letter, 1 lower case letter, 1 number and 1 special character. '
            )
		),
		'confirm_password' => array(
			'identical' => array(
                 'rule' => array('isIdentical', 'password'),
                 'message' => "Confirm password doesn't match"
            )
		),
		'phone' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณากรอกหมายเลขโทรศัพท์',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Phone number is invalid')
		),
		'role_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'กรุณาเลือกประเภทผู้ใช้'
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Result' => array(
			'className' => 'Result',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Form' => array(
			'className' => 'Form',
			'joinTable' => 'forms_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'form_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	public function isIdentical($first, $second) {
		return $first['confirm_password'] == $this->data['User'][$second];
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['User']['password'])) {
			$passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
			$this->data['User']['password'] = $passwordHasher->hash($this->data['User']['password']);
		} else {
			unset($this->data['User']['password']);
		}
		return true;
	}

	public function beforeValidate($options = array()) {
		//debug($this->data);
		if (!empty($this->id)) {
			if (empty($this->data['User']['password'])) {
				$this->validator()->remove('password');
			}
		}
		return true;
	}
}
