<div class="page-wrapper" id="banner">
	<div class="row text-center">
		<h3>
			<?php echo __($this->fetch('title'));?>
		</h3>
	</div>
	<div class="row">
		<div class="col-lg-8 center">
			<div class="well center">
				<?php echo $this->Form->create('User', array(
					'inputDefaults' => array(
						'div' => 'form-group',
						'class'=> 'form-control',
						'required' => false,
						'label' => array(
							'class' => 'col-lg-3 control-label'
						),
						'wrapInput' => 'col col-lg-9'
					),
					'class' => 'form-horizontal'
				)
				); ?>
				<?php
					//debug($this->request->data);
					echo $this->Form->input('id');
					echo $this->Form->input('username', array('label' => 'ชื่อผู้ใช้'));
					echo $this->Form->input('password', array('label' => 'รหัสผ่าน'));
					echo $this->Form->input('confirm_password', array(
						'type' => 'password',
						'label' => 'ยืนยันรหัสผ่าน'
						)
					);
					echo '<hr>';
					echo $this->Form->input('title', array(
						'label' => 'คำนำหน้าชื่อ',
						'wrapInput' => 'col col-lg-3'
						)
					);
					echo $this->Form->input('name', array('label' => 'ชื่อ'));
					echo $this->Form->input('surname', array('label' => 'นามสกุล'));
					echo $this->Form->input('institution', array('label' => 'หน่วยงาน'));
					echo $this->Form->input('position', array(
						'options' => $positions,
						'empty' => '-- เลือกตำแหน่ง --',
						'label' => 'ตำแหน่ง'
						)
					);
					echo $this->Form->input('phone', array('label' => 'เบอร์โทรศัพท์'));
					echo $this->Form->input('email', array('label' => 'อีเมล'));
					echo '<hr>';
					echo $this->Form->input('role_id', array(
						'options' => $roles,
						'empty' => '-- เลือกประเภทผู้ใช้ --',
						'label' => 'ประเภทผู้ใช้')
					);
				?>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-8 center text-right">
			<?php echo $this->Form->button($this->fetch('button'), array(
					    'type' => 'submit',
					    'escape' => true,
					    'class' => 'btn btn-primary'
						)
			);?>
			<?php echo $this->Html->link(__('ยกเลิก'), array(
				'controller' => 'users',
				'action' => 'index'
				), 
				array(
			    	'class' => 'btn btn-default'
			    )
			);?>
			<?php echo $this->Form->end();?>	
		</div>	
	</div>
</div>