<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?php echo "Homestay"; ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->fetch('meta');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('custom');
		echo $this->fetch('css');
	?>
	<link href="/homestay/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
	<?php echo $this->element('navbar');?>
	<div class="container">
		<?php echo $this->fetch('content'); ?>
	</div>
<?php
	echo $this->Html->script('jquery-2.1.0.min');
	echo $this->Html->script('bootstrap.min');
	//echo $this->Html->script('plugins/metisMenu/jquery.metisMenu');
	echo $this->fetch('scriptBottom');
	//echo $this->Html->script('sb-admin');
	echo $this->fetch('script');
	
?>	
</body>
</html>
