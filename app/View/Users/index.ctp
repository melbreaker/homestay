<div class="page-wrapper">
	<div class="row text-center">
		<h3>
			<?php echo __('จัดการผู้ใช้');?>
		</h3>
	</div>
	<div class="row">
		<?php echo $this->Session->flash();?>
		<p>
        <?php
        echo $this->Paginator->counter(array(
        'format' => __('{:page} of {:pages}')));
        ?>
    </p>
		<div class="panel panel-default">
		  <table class="table">
		    <thead>
		    	<th>ลำดับ</th>
		    	<th>ประเภทผู้ใช้</th>
		    	<th><?php echo $this->Paginator->sort('name', 'ชื่อ');?></th>
		    	<th><?php echo $this->Paginator->sort('surname', 'นามสกุล');?></th>
		    	<th><?php echo $this->Paginator->sort('institution', 'หน่วยงาน');?></th>
		    	<th>ตำแหน่ง</th>
		    	<th>เบอร์</th>
		    	<th class="text-center">
		    		<?php echo $this->Html->link(__('เพิ่ม +'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-success center'));?>
		    	</th>
		    </thead>
		    <tbody>
		    <?php 
		    	$i = $this->Paginator->counter(array('format' => __('{:start}')));
		    	foreach($users as $user):
		    ?>
		    	<tr>
		    		<td><?php echo $i;?></td>
		    		<td><?php echo $roles[$user['User']['role_id']];?></td>
		    		<td><?php echo $user['User']['name'];?></td>
		    		<td><?php echo $user['User']['surname'];?></td>
		    		<td><?php echo $user['User']['institution'];?></td>
		    		<td><?php echo $positions[$user['User']['position']];?></td>
		    		<td><?php echo $user['User']['phone'];?></td>
		    		<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-info')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', $user['User']['name']." ".$user['User']['surname'])); ?>
						
					</td>
		    	</tr>
		    <?php 
		    	$i += 1;
		    	endforeach;
		    ?>
		    </tbody>
		  </table>
		</div>
		<?php echo $this->Paginator->pagination(array('ul' => 'pagination pagination-right'));?>
	</div>
</div>