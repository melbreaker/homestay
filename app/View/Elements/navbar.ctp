 <div class="navbar navbar-default navbar-static-top ">
      <div class="container">
        <div class="navbar-header">
          <a href="../" class="navbar-brand">Homestay</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav nav-pills navbar-right custom-middle">
              <li <?php if($this->params['controller'] == 'users') echo 'class="active"';?>><?php echo $this->Html->link(__('จัดการผู้ใช้'), array('controller' => 'users', 'action' => 'index'));?></li>
              <li <?php if($this->params['controller'] == 'forms') echo 'class="active"';?>><?php echo $this->Html->link(__('จัดการฟอร์มประเมิน'), array('controller' => 'forms', 'action' => 'index'));?></li>
              <li <?php if($this->params['controller'] == 'homestays') echo 'class="active"';?>><?php echo $this->Html->link(__('จัดการกลุ่มบ้านพัก'), array('controller' => 'homestays', 'action' => 'index'));?></li>
              <li <?php if($this->params['controller'] == 'results') echo 'class="active"';?>><?php echo $this->Html->link(__('ผลการประเมิณ'), array('controller' => 'results', 'action' => 'index'));?></li>
              <li><?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));?></li>
            </ul>
        </div>
      </div>
    </div>