<div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.html"><i class="fa fa-user fa-fw"></i> <?php echo __("จัดการผู้ประเมิน")?></a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> จัดการแบบฟอร์มประเมินผล</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-wrench fa-fw"></i> Admin</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>