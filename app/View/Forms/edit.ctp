<?php echo $this->Html->script(array('ckeditor/ckeditor', 'ckeditor/adapters/jquery', 'ck-in-place'), array('block' => 'scriptBottom'));?>
<div class="page-wrapper">
	<div class="row text-center">
		<h3><?php echo __('จัดการแบบฟอร์มการประเมิน');?></h3>
	</div>
	<div class="row text-center">
		<h4>
			<div class="title editable bottomTooltip" data-toggle="tooltip" data-original-title="ดับเบิลคลิกเพื่อแก้ไข">
				<?php echo __('แบบฟอร์มการประเมินมาตรฐานโฮมสเตย์ ปี พ.ศ. ๒๕๕๗')." ";?>
				 <span class="glyphicon glyphicon-edit custom-gli"></span>
			</div>
		</h4>
	</div>
	<br>
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-lg-3 custom-sidebar">
					<div class="col-lg-9 compact">
						<h5><?php echo __('เกณฑ์มาตรฐาน');?></h5>
					</div>
					<div class="col-lg-3 compact text-right">
						<?php echo $this->Html->link('เพิ่ม +', array('controller' => 'form', 'action' => 'add'), array('class' => 'btn btn-success'));?>
					</div>
					<div class="col-lg-12 compact">
						<ul class="nav nav-pills nav-stacked small-padding-top">
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li class="active"><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						  <li><a href="#">1. มาตรฐานที่ 1</a></li>
						</ul>						
					</div>
				</div>
				<div class="col-lg-9 page-with-sidebar">
					<h5>
						<div class="category editable leftTooltip" data-toggle="tooltip" data-original-title="ดับเบิลคลิกเพื่อแก้ไข">
							<?php echo __('เกณฑ์มาตรฐานที่สอง')." ";?>
							<span class="glyphicon glyphicon-edit custom-gli"></span>
						</div>
					</h5>
					<div class="panel panel-default">
						<table class="table table-bordered">
							<thead style="background-color:#999999;">
								<th class="vertical-middle white">ลำดับ</th>
								<th class="vertical-middle white">ตัวชี้วัด</th>
								<th class="vertical-middle white">วิธีการวัดประเมิน</th>
								<th class="vertical-middle white">น้ำหนัก</th>
								<th class="vertical-middle white">ระดับคุณภาพ</th>
								<th class="text-center"><?php echo $this->Html->link('เพิ่ม +', array('controller' => 'forms', 'action' => 'add'), array('class' => 'btn btn-success'));?></th>
							</thead>
							<tbody>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
								<tr>
									<td>1.1</td>
									<td>ลักษณะบ้านพักที่เป็นสัดส่วน</td>
									<td style="max-width: 250px;">เป็นบ้านของเจ้าของที่แบ่งปันที่นอนหรือห้องนอน อย่างเป็นสัดส่วน หรืออาจปรับปรุง ต่อเติมที่พัก ที่ติดกับบ้านเดิมเพื่อใช้เป็นที่นอนหรือห้องนอนเป็นสัดส่วน</td>
									<td>2</td>
									<td>1 - 5</td>
									<td class="actions text-center">
						<?php echo $this->Html->link(__('แก้ไข'), '#', array('class' => 'btn btn-info btn-sm')); ?>
						<?php echo $this->Form->postLink(__('ลบ'), '#', array('class' => 'btn btn-danger'), __('Are you sure you want to delete "%s"?', 'this')); ?>
						
					</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>