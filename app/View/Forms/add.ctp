<?php echo $this->Html->script(array('ckeditor/ckeditor', 'ckeditor/adapters/jquery', 'ck-in-place', 'form/individual_form'), array('block' => 'scriptBottom'));?>
<div class="page-wrapper">
	<div class="row text-center">
		<h3><?php echo __('จัดการแบบฟอร์มการประเมิน');?></h3>
	</div>
	<div class="row text-center">
		<h4>
			<div class="title editable bottomTooltip" data-toggle="tooltip" data-original-title="ดับเบิลคลิกเพื่อแก้ไข">
				 <?php 
				 	if (isset($form['Form']['title']) && !empty($form['Form']['title'])) {
				 		echo h($form['Form']['title'].' ');
				 	} else {
				 		echo __('ชื่อแบบฟอร์ม ');
				 	}
				 ?>
				 <span class="glyphicon glyphicon-edit custom-gli"></span>
			</div>

		</h4>
	</div>
	<?php echo $this->Form->hidden('id', array('id' => 'form-id', 'default' => $form['Form']['id']));?>
	<br>
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-lg-3 custom-sidebar">
					<div class="col-lg-9 compact">
						<h5><?php echo __('เกณฑ์มาตรฐาน');?></h5>
					</div>
					<div class="col-lg-3 compact text-right">
						<button class="btn btn-success" data-toggle="modal" data-target=".category-modal">เพิ่ม +</button>
					</div>
					<div class="col-lg-12 compact">
						<ul class="nav nav-pills nav-stacked" id="category-list">
						<?php foreach($categories as $category):?>
						   <li><a href="#"><?php echo $category['Category']['title'];?></a></li>
						<?php endforeach;?>
						</ul>
					</div>
				</div>
				<div class="col-lg-9 page-with-sidebar">
					<div class="col-lg-12 compact text-center lightGrey"><h5><i><?php echo __('ยังไม่มีข้อมูล');?></i></h5></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="category-modal" class="modal fade category-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
    		<h4 class="modal-title">เพิ่มเกณฑ์มาตรฐาน</h4>
    	</div>
    	<div class="modal-body">
    		<input type="text" class="form-control" id="category-input">
    	</div>
    	<div class="modal-footer">
    		<button type="button" id="category-submit" class="btn btn-primary">ตกลง</button>
    		<button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
    	</div>
    </div>
  </div>
</div>	