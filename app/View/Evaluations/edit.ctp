<div class="evaluations form">
<?php echo $this->Form->create('Evaluation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Evaluation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('no');
		echo $this->Form->input('indicator');
		echo $this->Form->input('evaluation');
		echo $this->Form->input('weight');
		echo $this->Form->input('evaluation_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Evaluation.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Evaluation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Evaluations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Evaluations'), array('controller' => 'evaluations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Evaluation'), array('controller' => 'evaluations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
	</ul>
</div>
