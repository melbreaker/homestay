<?php
App::uses('AppController', 'Controller');
/**
 * Forms Controller
 *
 * @property Form $Form
 * @property PaginatorComponent $Paginator
 */
class FormsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler');
	public $uses = array('Form', 'Category');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$form = $this->Form->find('first');
		if (!$form) {
			$data = array(
				'Form' => array(
					'title' => '',
					'status' => false
					)
				);
			$this->Form->save($data);
		} 
			
		$categories = $this->Category->find('all', array(
			'conditions' => array('Category.form_id' => $form['Form']['id'])
			)
		);

		$this->set(compact('form'));
		$this->set(compact('categories'));
		$this->render('add');
	}

	public function editFormTitle() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$form = $this->Form->find('first');
			$this->Form->id = $form['Form']['id'];
			if($this->Form->saveField('title', strip_tags($this->request->data['content']))) {
				$result = array('error' => true, 'content' => '');
			}
			$result = array('error' => true , 'content' => 'ไม่สามารถแก้ไขชื่อฟอร์มได้ โปรดลองอีกครั้ง');
		}
		$this->set(compact($result));
		$this->set('_serialize', 'result');
		exit;
	}

	public function addCategory(){
		if($this->request->is('ajax')) {
			$data = array(
				'Category' => array(
					'title' => $this->request->data['category'],
					'form_id' => $this->request->data['id'],
				)
			);

			$error = $this->Category->invalidFields();
			if (empty($error)) {
				$result = $this->Category->save($data);
				if ($result) {
					$this->Category->recursive = -1;
					$categories = $this->Category->find('all');
					$this->api_result(true, array('data' => $categories));
				} else {
					$this->api_result(false, array('cause' => 'ไม่สามารถเพิ่มหัวข้อได้'));
				}
			} else {
				$this->api_result(false, array('cause' => $error));
			}
		}
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Form->exists($id)) {
			throw new NotFoundException(__('Invalid form'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Form->save($this->request->data)) {
				$this->Session->setFlash(__('The form has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The form could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Form.' . $this->Form->primaryKey => $id));
			$this->request->data = $this->Form->find('first', $options);
		}
		$homestays = $this->Form->Homestay->find('list');
		$users = $this->Form->User->find('list');
		$this->set(compact('homestays', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Form->id = $id;
		if (!$this->Form->exists()) {
			throw new NotFoundException(__('Invalid form'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Form->delete()) {
			$this->Session->setFlash(__('The form has been deleted.'));
		} else {
			$this->Session->setFlash(__('The form could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
