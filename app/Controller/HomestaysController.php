<?php
App::uses('AppController', 'Controller');
/**
 * Homestays Controller
 *
 * @property Homestay $Homestay
 * @property PaginatorComponent $Paginator
 */
class HomestaysController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Homestay->recursive = 0;
		$this->set('homestays', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Homestay->exists($id)) {
			throw new NotFoundException(__('Invalid homestay'));
		}
		$options = array('conditions' => array('Homestay.' . $this->Homestay->primaryKey => $id));
		$this->set('homestay', $this->Homestay->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Homestay->create();
			if ($this->Homestay->save($this->request->data)) {
				$this->Session->setFlash(__('The homestay has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homestay could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Homestay->exists($id)) {
			throw new NotFoundException(__('Invalid homestay'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Homestay->save($this->request->data)) {
				$this->Session->setFlash(__('The homestay has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homestay could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Homestay.' . $this->Homestay->primaryKey => $id));
			$this->request->data = $this->Homestay->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Homestay->id = $id;
		if (!$this->Homestay->exists()) {
			throw new NotFoundException(__('Invalid homestay'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Homestay->delete()) {
			$this->Session->setFlash(__('The homestay has been deleted.'));
		} else {
			$this->Session->setFlash(__('The homestay could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
