<?php
App::uses('AppController', 'Controller');
/**
 * FormsUsers Controller
 *
 * @property FormsUser $FormsUser
 * @property PaginatorComponent $Paginator
 */
class FormsUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FormsUser->recursive = 0;
		$this->set('formsUsers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FormsUser->exists($id)) {
			throw new NotFoundException(__('Invalid forms user'));
		}
		$options = array('conditions' => array('FormsUser.' . $this->FormsUser->primaryKey => $id));
		$this->set('formsUser', $this->FormsUser->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FormsUser->create();
			if ($this->FormsUser->save($this->request->data)) {
				$this->Session->setFlash(__('The forms user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forms user could not be saved. Please, try again.'));
			}
		}
		$users = $this->FormsUser->User->find('list');
		$forms = $this->FormsUser->Form->find('list');
		$this->set(compact('users', 'forms'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FormsUser->exists($id)) {
			throw new NotFoundException(__('Invalid forms user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FormsUser->save($this->request->data)) {
				$this->Session->setFlash(__('The forms user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forms user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FormsUser.' . $this->FormsUser->primaryKey => $id));
			$this->request->data = $this->FormsUser->find('first', $options);
		}
		$users = $this->FormsUser->User->find('list');
		$forms = $this->FormsUser->Form->find('list');
		$this->set(compact('users', 'forms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FormsUser->id = $id;
		if (!$this->FormsUser->exists()) {
			throw new NotFoundException(__('Invalid forms user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FormsUser->delete()) {
			$this->Session->setFlash(__('The forms user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The forms user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
