<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {
	public $uses = array('User', 'Group');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->Paginator->settings = array(
			'limit' => 2,
			'conditions' => array('User.id <>' => $this->Auth->user('id')),
			'order' => array(
			'User.id' => 'asc'
			)
		);
		$this->set('users', $this->Paginator->paginate('User'));
		$roles = $this->Group->find('list');
		$this->set(compact('roles'));

		$positions = array('ตำแหน่งที่ 1', 'ตำแหน่งที่ 2', 'ตำแหน่งที่ 3', 'ตำแหน่งที่ 4', 
			'ตำแหน่งที่ 5', 'ตำแหน่งที่ 6', 'ตำแหน่งที่ 7', 'ตำแหน่งที่ 8', 'ตำแหน่งที่ 9', 'ตำแหน่งที่ 10');
		$this->set(compact('positions'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success alert-dismissable'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger alert-dismissable'));
			}
		}
		$titles = array('นาย', 'นาง', 'นางสาว');
		$positions = array('ตำแหน่งที่ 1', 'ตำแหน่งที่ 2', 'ตำแหน่งที่ 3', 'ตำแหน่งที่ 4', 
			'ตำแหน่งที่ 5', 'ตำแหน่งที่ 6', 'ตำแหน่งที่ 7', 'ตำแหน่งที่ 8', 'ตำแหน่งที่ 9', 'ตำแหน่งที่ 10');
		$roles = $this->Group->find('list');

		$this->set(compact('titles'));
		$this->set(compact('positions'));
		$this->set(compact('roles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The User has been saved.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success alert-dismissable'
            ));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger alert-dismissable'
            	));
			}
		} else {
			$options = array(
				'conditions' => array('User.' . $this->User->primaryKey => $id),
			);
			$this->request->data = $this->User->find('first', $options);
			unset($this->request->data['User']['password']);
		}
		$titles = array('นาย', 'นาง', 'นางสาว');
		$positions = array('ตำแหน่งที่ 1', 'ตำแหน่งที่ 2', 'ตำแหน่งที่ 3', 'ตำแหน่งที่ 4', 
			'ตำแหน่งที่ 5', 'ตำแหน่งที่ 6', 'ตำแหน่งที่ 7', 'ตำแหน่งที่ 8', 'ตำแหน่งที่ 9', 'ตำแหน่งที่ 10');
		$roles = $this->Group->find('list');
		$this->set(compact('titles'));
		$this->set(compact('positions'));
		$this->set(compact('roles'));
	}	

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function login() {
		$this->layout = 'login';
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl($this->Auth->loginRedirect));
			}
			$this->Session->setFlash(__('Invalid username or password, try again '), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger alert-dismissable'
            ));
		}
	}
	
	public function logout() {
		$this->Session->setFlash(__('ออกจากระบบเรียบร้อยแล้ว '), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success alert-dismissable'
        ));
		return $this->redirect($this->Auth->logout());
	}
}
